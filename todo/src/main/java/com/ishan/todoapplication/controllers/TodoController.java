package com.ishan.todoapplication.controllers;


import com.ishan.todoapplication.models.Todo;
import com.ishan.todoapplication.repositories.TodoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class TodoController {

    @Autowired
    TodoRepository todoRepository;


    @RequestMapping(value = "/todos", method = RequestMethod.GET)
    public List<Todo> getAllTodos() {
        Sort sortByCreatedAtDesc = new Sort(Sort.Direction.DESC, "createdAt");
        return todoRepository.findAll(sortByCreatedAtDesc);
    }

    @RequestMapping(value = "/todos", method = RequestMethod.POST)
    public Todo createTodo(@Valid @RequestBody Todo todo) {
        todo.setCompleted(false);
        return todoRepository.save(todo);
    }

    @RequestMapping(value = "/todos/{id}", method = RequestMethod.GET)
    public ResponseEntity<Todo> getTodoById(@PathVariable("id") String id, @Valid @RequestBody Todo todo) {
        Todo todoData = todoRepository.findOne(id);
        if (null == todoData) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(todo, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/todos/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Todo> updateTodo(@PathVariable("id") String id, @Valid @RequestBody Todo todo) {
        Todo todoData = todoRepository.findOne(id);
        if (null == todoData) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            todoData.setTitle(todo.getTitle());
            todoData.setCompleted(todo.getCompleted());
            Todo updatedTodo = todoRepository.save(todoData);
            return new ResponseEntity<>(updatedTodo,HttpStatus.OK);
        }
    }

    @RequestMapping(value="/todos/{id}",method=RequestMethod.DELETE)
    public void deleteTodo(@PathVariable("id") String id) {
        todoRepository.delete(id);
    }
}
