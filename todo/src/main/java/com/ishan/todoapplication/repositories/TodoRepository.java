package com.ishan.todoapplication.repositories;

import com.ishan.todoapplication.models.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TodoRepository extends MongoRepository<Todo,String> {
}
