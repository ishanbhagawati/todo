# TODO app built using Spring Boot, MongoDB, Angular-4

Build a Fully-Fledged Todo App with Spring Boot & MongoDB in the Backend and Angular 4 in the frontend.

## Requirements

1. Java - 1.8.x

2. Maven - 3.x.x

3. MongoDB - 3.x.x to 

## Steps to Setup

**1. update mongo DB properties**
```java
spring.data.mongodb.uri=mongodb://localhost:27017/todoapp
```


**2. Build and run the backend app using maven**

```bash
mvn spring-boot:run
```

The backend server will start at <http://localhost:8080>.

**3. Run the frontend app using npm**

```bash
cd frontend
npm install
```

```bash
npm start
```

Frontend server will run on <http://localhost:4200>

